# consulting-lab

A repository containing collection of resources used by consultants in their lab environments.  

RH Consultants are often faced with the situation where they need to test and lab with quite extensive scenarios that requires a customer like environment and should interact with many different components.
Things like:
- Locked down networks and proxies
- High Availability and 
- IdM/LDAP authentication
- Doing things at scale

While most of the stuff you find in here can be used adhoc, there is also an idea of spinning up a complete lab in short amount of time.  
The lab is based around the idea that you have a very _large_ host that will act as your hypervisor, firewall, proxy for the virtual networks.    
As of right now this is performed with libvirt and iptables and configured using multiple Ansible roles in combination.  

The goal is for you to be able to describe the labenvironment in a configuration file and press the button.   
For the automation we use ansible. We model the environment using ansible variables.    
Since it's a lot to configure we try to keep the defaults sensible and plenty.  

You find more instructions on how to create your own lab below.

**NOTE: This is not yet stable and rapid changes are to be expected at the moment**

## Roles in this repository

| role                                                    | description                                                                           |
| ------------------------------------------------------- | ------------------------------------------------------------------------------------- |
| [cdnmirror](/roles/cdnmirror)                           | sets up a simple CDN mirror on a VM                                                   |
| [hetzner-post-provision](/roles/hetzner-port-provision) | role used for post provision tasks                                                    |
| [hetzner-provision](/roles/hetzner-provision)           | initial provision tasks when hetzner machine is in rescue mode                        |
| [knockd](/roles/knockd)                                 | configure port knocking                                                               |
| [libvirt](/roles/libvirt)                               | installs and configures libvirt 
| [libvirt-network](/roles/libvirt-network/)              | creates the libvirt networks from the `libvirt_networks:` defined in inventory        |
| [libvirt-iptables](/roles/libvirt-iptables)             | role to configure routing between networks created with libvirt_network               |
| [mdadm-sync](/roles/mdadm-sync)                         | pause and start mdadm RAID sync                                                       |
| [motd](/roles/motd)                                     | sets a motd on the target machine                                                     |
| [openvpn](/roles/openvpn)                               | sets up a static openvpn server                                                       |
| [reboot](/roles/reboot)                                 | reboots the target machine and waits for it come back                                 |
| [rhel-iso-download](/roles/rhel-iso-download)           | downloads images from redhat CDN. requires active subscription (see `subman`)         |
| [subman](/roles/subman)                                 | register and attaches subscription if not present                                     |
| [users](/roles/users)                                   | creates local users with authorized_keys and sudo                                     |
| [squid](/roles/squid)                                   | install a squid proxy                                                                 |
| [vm-template](/roles/vm-template)                       | creates an up-to-date rhel7.6 template image. deps: `subman` and `rhel-iso-download`  |
| [vm-create](/roles/vm-create)                           | creates VM's acord. to spec. Supports static ip and extra disks. deps: `vm-template`  |
| [reposync](/roles/reposync)(DEPRECATED)                 | creates a rhel7 repomirror on the labhost. requires active subscription (see `subman`)|

# Get Started


Order the server bla bla.. 

## Before you begin

The roles and overall solution has been developed and tested with Ansible 2.7.  
Make sure that you use Ansible 2.7 or later. 
```
$ ansible --version
```


WIP: 
The roles have some dependencies that you don't get out of the box with ansible.  
Make sure to install them:
```
$ pip install requirements.yml.xml ??
```

## Checkout the repository locally

```
$ git clone 
$ cd consulting-lab
```



## Setup you environment in inventory/labenv

The idea here is to model up your lab environment within the configuration files. Infrastructure as Code if you will.  
When the environment grows or changes, we edit the configuration and run Ansible.  

Were trying out the concept of using multiple inventory sources that becomes a flattened inventory at runtime. (See: [Using Inventory Directories and Multiple Inventory Sources](https://docs.ansible.com/ansible/latest/user_guide/intro_dynamic_inventory.html#using-inventory-directories-and-multiple-inventory-sources)  
Everything is kept within the `inventory/` directory.

It consists of two parts: the `inventory/labenv` and the `inventory/connections`.


`inventory/labenv` is in the YAML format, which allows us to add lists and dicts in a nicer way than INI files.  
For some it's nicer to keep the whole environment within one file, instead of spreading it out over group_vars and host_vars.  

However, each to his own and nothing stopping you from using the separate vars folders.  
An _alternative_ way would be to create `group_vars/` and `host_vars/` within the `inventory/` dir.   



`inventory/connections` is manipulated by the `vm-create` role.  
When it creates a new host, the connection details are added here for future connections!
This way we dynamically add and remove parts of the inventory.  
It contains the jumphost configuration for all VM's so that you can connect to the `hosts:`.  

**We encourage you to read more about `inventory/labenv` and `inventory/connections` [here](/docs/inventory.md)**



### Add the labhost to inventory

First step should be to put your labhost in the labhost group.

Optional, while neat, is to first put your labhost ip in `/etc/hosts`.  
Useful when your host is not resolvable.

```
# /etc/hosts
12.34.56.78 labhost
```

Add the hostname of your labhost to the `[labhost]` group in the inventory/labhost file.  

(AT THE MOMENT)  
Edit inventory/labhost
```
# inventory/labhost
[labhost]
labhost
```

In this example we use the same name for both the group and and the host.  
But here you can define you alternative hostname. 

```
# inventory/labhost
[labhost]
thehost.my.lab.org
```

### Prepare the labhost

If you are planning to use a Hetzner server, we have a quick guide on that [here](/docs/hetzner-setup.md).  
We have roles that can install a Hetzner server booted into rescue/setup mode from scratch.  

The requirement for the labhost is that it's a clean installation of either Red Hat Enterprise Linux 7 or CentOS 7. 

The host should prefferanly habe the user you plan to use as a NOPASSWD sudo user. HAving your public-key added to `authorized_keys` is prefferable.   

Verify that you can connect to the host:
```
[myuser@laptop consulting-lab]$ ansible -m ping labhost -i inventory/
labhost | SUCCESS => {
    "changed": false, 
    "ping": "pong"
}

```

### Users:

First we define the users for the [`users`](/role/users) role.  

It is _recommended_ to create the user as you are using on your workstation.  
This so that you can connect effortlessly once everything is in place and the playbooks and roles have been developed with this setting.  

Edit the `inventory/labenv`
```
[myuser@laptop consulting-lab]$ cat inventory/labenv

all:
  hosts:
    labhost
  vars:
    users:
    - name: myuser
      public_key: "ssh-rsa BBBBB3NzaC1yc2EAAAADAQABAAABAQCy7mCSSm7Nh1pA2WuurTdbGmiY6RyqxbAXNMZAo9df/kHwJIfFKQQIoZPyvgfbW3P+hoFTL1x1SkOFQLSKCGg6DIQwMhbxMMkhUvoeg8mMiGikdD3uPGqlWb0gjf8HaWhyaFsmwUSrC6WqgAsd9nHF682Cx4yU+olD43JLpDj35L+vx02TvVScBhseSWwCCsATZBxEpIg/BtMyT5bXF6WjJL8PTzNA+xY5+OoPM1d+JasFb28M+Gxj9pjD4xFT/MR5Rvaor/GiooX+7jxZubi6b0sEfvkgLkCol2y69ptAhIEAk+qzKwBVseuyGjOQrjbK0KQXFy6xfOyFZwqK/ofz"
```

By default the role creates the user with NOPASSWD sudo.  
Later, when we create the VM template the users defined here will be included in the build.  

### subscription-manager

We will register the labhost and the VM's to Red Hat.  
This is done by the `subman` role and is used as a dependency in many places.  

Define the Red Hat username and password: 

```
all:
  hosts:
    labhost
  vars:
    rh_username: consultant@redhat.com
    rh_password: supersecret
```

Here you can make use of [Ansible Vault]() and the `encrypt_string`. It does however require you to supply the vaultpw everytime you run playbooks.  
```
all:
  hosts:
    labhost
  vars:
    rh_username: consultant@redhat.com
    rh_password: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          33666233646236633333656137636231303331303935313063356238653833353566616236366165
          6135346365323131363232383736383861373564353634340a623432323439363861333838383730
          35636235323564346238393762363465373036623532363964326463343262623036353338646337
          3061336535656631370a383631363162623539646362343632626233663737313062633864316437
          6336
``` 

### networks

Now we will define the networks that should be created in virtual environment.  
Read up the [networking part]() and the different network modes that are supported.

We define networks in the list `libvirt_networks:`.

```
all:
  vars:
    ---

    libvirt_networks:

``` 
Every list item here will be created when we run the role `libvirt-network`.  
A network entry requires a `name:`, `subnet:` and `type:`.
`type:` will default to `restricted` if not supplied. 

```
all:
  vars:
    ---

    libvirt_networks:
    - name: management
      subnet: 10.50.0.0/24
      type: restricted

    # internet access
    - name: nat-network
      subnet: 172.16.122.0/24
      type: nat
``` 

The type restricted means that the network is locked down and you have to open up for ports that you need (like in a real secure environment).
You can assign these port openings on the network entry with `port_openings`: 

```
all:
  vars:
    ---

    libvirt_networks:
    - name: management
      subnet: 10.50.0.0/24
      type: restricted
      port_openings:
      - port: 53
        proto: tcp
        comment: "Allow DNS from everywhere"
      - port: 53
        proto: udp 
        comment: "Allow DNS from everywhere"

    # internet access
    - name: nat-network
      subnet: 172.16.122.0/24
      type: nat
``` 

Here we've opened up for _any_ network to access _any_ host on the `management` network on port 53 tcp + udp.  
```
0.0.0.0/0 --> 10.50.0.0/24:53/tcp
0.0.0.0/0 --> 10.50.0.0/24:53/udp
```


### virtual machines

With networks defined, we can define what Virtual Machines should be created on the networks.  
VM's are created with the role [vm-create](/roles/vm-create), and in order for that to work we have to have a template image.  
The template image is in turn created by [vm-template](/roles/vm-template).  

When we run the role [create-inventory-vms](/roles/create-inventory-vms) it will search the inventory for VM entrys specified in the GROUP(may change) [vms].
It will create them one by one from the specifications.o

The VM entry requires the following vars:

`name:`  
`network:` the name of the network created in previous step. 
`ip_address:` a valid ipv4 address that fits inside the range of the network specified.  

```
all:
  children:
    vms:
      hosts:
        # define virtual machines below
        vm1:
          name: vm1
          network: management
          ip_address: 10.50.0.40
``` 

We can modify the specs of the VM and add extra disk:

```
all:
  children:
    vms:
      hosts:
        # define virtual machines below
        vm1:
          name: vm1
          network: management
          ip_address: 10.50.0.40
          memory: 2048
          cpu: 2
          disk: 20G
          extra_disks:
          - size: 20G
          - size: 50G
            name: userdata
```


## Example setup

You'll find multiple setups of ready-to-go labenvironments in the [examples](/examples/) folder.  
Read more about each example in their README.md.   

You can run the setup directly from the folder itself or from the repository root.
```
cd examples/<example>/
$ ansible-playbook setup_lab.yml 

or

$ ansible-playbook examples/<example>/setup_lab.yml -i examples/<example>/inventory/ 
```

## 
